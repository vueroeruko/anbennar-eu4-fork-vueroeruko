owner = Y01
controller = Y01
add_core = Y01
culture = horned_ogre
religion = lefthand_path

hre = no

base_tax = 1
base_production = 1
base_manpower = 2

trade_goods = grain

capital = ""

is_city = yes
